package adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.development.hounge.wifiscantest.R;

import java.util.ArrayList;

/**
 * Created by hounge on 10/29/14.
 */
public class WifiAdapter extends BaseAdapter {

    private Activity activity;

    private static ArrayList title, notice, notice2;
    private static LayoutInflater inflater = null;

    public WifiAdapter(Activity a, ArrayList b, ArrayList bod, ArrayList bod2) {
        activity = a;
        this.title = b;
        this.notice = bod;
        this.notice2 = bod2;

        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return title.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        if (vi == null) {
            vi = inflater.inflate(R.layout.wifilistview, null);


            TextView title1 = (TextView) vi.findViewById(R.id.SSID);
            String signal = title.get(position).toString();
            title1.setText(signal);

            TextView title2 = (TextView) vi.findViewById(R.id.Capabilities);
            String ssid = notice.get(position).toString();
            title2.setText(ssid);

            TextView title3 = (TextView) vi.findViewById(R.id.Level);
            String security = notice2.get(position).toString();
            title3.setText(security);


            return vi;


        } else {
            
        }
        return vi;
    }
}
