package com.development.hounge.wifiscantest;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import adapters.WifiAdapter;

/**
 * Created by hounge on 10/29/14.
 */


public class ScanWifi extends Activity {
    private static final long SCAN_DELAY = 20000;
    private IntentFilter i;
    private BroadcastReceiver receiver;
    private List<ScanResult> list2;
    Timer timer;
    ArrayList title_array = new ArrayList<String>();
    ArrayList notice_array = new ArrayList<String>();
    ArrayList notice_array2 = new ArrayList<String>();
    ListView list;
    WifiAdapter adapter;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_wifi);
        list = (ListView) findViewById(R.id.listView2);
        i = new IntentFilter();
        i.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);

        final ListView list = getListView();
        list.setTextFilterEnabled(true);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // When clicked, show a toast with the TextView text
                Toast.makeText(getApplicationContext(), parent.getItemAtPosition(position).toString(),
                        Toast.LENGTH_SHORT).show();
            }

        });

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent i) {
                WifiManager w = (WifiManager) c.getSystemService(Context.WIFI_SERVICE);
                list2 = null;
                    list2 = w.getScanResults();
                    for (ScanResult r : list2) {
                        title_array.add(r.SSID);
                        notice_array.add(r.capabilities);
                        notice_array2.add(r.level);
                     }
                adapter = new WifiAdapter(ScanWifi.this, title_array, notice_array,notice_array2);
                list.setAdapter(adapter);

            }



        };
    }
    @Override
    protected void onResume() {
        super.onResume();

        timer = new Timer(true);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

            }
        }, 0, SCAN_DELAY);
        registerReceiver(receiver, i);
    }

    @Override
    protected void onPause() {
        super.onPause();

        timer.cancel();
    }


    public ListView getListView() {
        return list;
    }
}
